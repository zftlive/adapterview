package com.zftlive.share.adapterview.base;

import java.io.Serializable;

/**
 * 多类型数据模型接口定义
 *
 * @author 曾繁添
 * @version 1.0
 */
public interface IAdapterModel extends Serializable {

}

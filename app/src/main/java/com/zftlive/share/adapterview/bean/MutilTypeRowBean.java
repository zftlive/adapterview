package com.zftlive.share.adapterview.bean;

import java.io.Serializable;

/**
 * 多种类型列表数据模型
 *
 * @author 曾繁添
 * @version 1.0
 */
public class MutilTypeRowBean extends ButtonLinkBean {

    private static final long serialVersionUID = 912453293774487205L;

    public MutilTypeRowBean() {
    }

    public MutilTypeRowBean(int itemType, ArticleRowData article) {
        super.itemType = itemType;
        this.article = article;
    }

    public MutilTypeRowBean(int itemType, ButtonLinkBean buttonLink) {
        super.itemType = itemType;
        this.buttonLink = buttonLink;
    }

    /**
     * 文章模型
     */
    public ArticleRowData article;

    /**
     * 按钮数据模型
     */
    public ButtonLinkBean buttonLink;

}

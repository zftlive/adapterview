package com.zftlive.share.adapterview.bean;

import java.io.Serializable;

/**
 * 文章数据模型
 *
 * @author 曾繁添
 * @version 1.0
 */
public class ArticleRowData extends BaseAdapterModel {

    private static final long serialVersionUID = 1424462076358736360L;

    /**
     * 文章id
     */
    public String id = "";

    /**
     * 作者头像
     */
    public String authorImg = "";

    /**
     * 作者名称
     */
    public String authorName = "";

    /**
     * 作者id
     */
    public String authorId = "";

    /**
     * 发布时间
     */
    public String releaseDT = "";

    /**
     * 文章缩略图
     */
    public String thumb = "";

    /**
     * 文章标题
     */
    public String title = "";

    /**
     * 文章简介
     */
    public String summary = "";

    /**
     * 文章分类标签
     */
    public String tag = "";
}

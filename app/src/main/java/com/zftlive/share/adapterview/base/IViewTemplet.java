package com.zftlive.share.adapterview.base;

import android.view.View;

/**
 * 模板基础抽象接口定义
 *
 * @author 曾繁添
 * @version 1.0
 */
public interface IViewTemplet {

    /**
     * 绑定布局文件
     * @return
     */
    int bindLayout();

    /**
     * 代码创建View与bindLayout二选一
     * @return
     */
    View bindView();

    /**
     * 查找控件
     */
    void initView();

    /**
     * 填充每一行数据
     * @param model 当前行数据模型
     * @param position 数据索引位置
     */
    void fillData(IAdapterModel model, int position);

    /**
     * 获取当前item的View视图
     * @return
     */
    View getItemLayoutView();
}

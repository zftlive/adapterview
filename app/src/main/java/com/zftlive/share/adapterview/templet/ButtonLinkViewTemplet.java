/*
 *     Android基础开发个人积累、沉淀、封装、整理共通
 *     Copyright (c) 2016. 曾繁添 <zftlive@163.com>
 *     Github：https://github.com/zengfantian || http://git.oschina.net/zftlive
 *
 *     Licensed under the Apache License, Version 2.0 (the "License");
 *     you may not use this file except in compliance with the License.
 *     You may obtain a copy of the License at
 *
 *         http://www.apache.org/licenses/LICENSE-2.0
 *
 *     Unless required by applicable law or agreed to in writing, software
 *     distributed under the License is distributed on an "AS IS" BASIS,
 *     WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *     See the License for the specific language governing permissions and
 *     limitations under the License.
 *
 */

package com.zftlive.share.adapterview.templet;

import android.content.Context;
import android.graphics.Color;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.zftlive.share.adapterview.GlobalApplication;
import com.zftlive.share.adapterview.base.AbsViewTemplet;
import com.zftlive.share.adapterview.base.IAdapterModel;
import com.zftlive.share.adapterview.bean.MutilTypeRowBean;

import share.zftlive.com.adapterview.R;

/**
 * 按钮基础视图模板
 *
 * @author 曾繁添
 * @version 1.0
 *
 */
public class ButtonLinkViewTemplet extends AbsViewTemplet {

    /**
     * 图标
     */
    private ImageView mIcon;

    /**
     * 按钮文案
     */
    private TextView mButtonText;

    /**
     * 图片显示option
     */
    protected DisplayImageOptions mExactlyOption;

    public ButtonLinkViewTemplet(Context mContext) {
        super(mContext);
        mExactlyOption = GlobalApplication.gainExactlyOption(R.mipmap.common_module_default_picture);
    }

    @Override
    public int bindLayout() {
        return R.layout.element_item_button_link;
    }

    @Override
    public void initView() {
        mIcon = (ImageView)findViewById(R.id.iv_button_icon);
        mButtonText = (TextView) findViewById(R.id.tv_button_text);
    }

    @Override
    public void fillData(IAdapterModel model, int position){
        MutilTypeRowBean rowBean = (MutilTypeRowBean) model;
        if (null == rowBean || rowBean.buttonLink == null) {
            Log.e(TAG, position + "-->数据为空");
            return;
        }
        //复位默认图片
        mIcon.setImageResource(R.mipmap.common_module_default_picture);
        if(!TextUtils.isEmpty(rowBean.buttonLink.iconURL)){
            ImageLoader.getInstance().displayImage(rowBean.buttonLink.iconURL,mIcon,mExactlyOption);
        }
        mIcon.setVisibility(TextUtils.isEmpty(rowBean.buttonLink.iconURL)?View.GONE:View.VISIBLE);
        mButtonText.setText(rowBean.buttonLink.buttonText);
        mButtonText.setTextColor(Color.parseColor(rowBean.buttonLink.buttonTextColor));
    }
}

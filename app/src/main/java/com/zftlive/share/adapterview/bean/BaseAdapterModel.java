package com.zftlive.share.adapterview.bean;

import com.zftlive.share.adapterview.base.IAdapterModel;

/**
 * Adapter数据模型基类
 *
 * @author 曾繁添
 * @version 1.0
 */
public class BaseAdapterModel implements IAdapterModel {

    private static final long serialVersionUID = -3599229691363964123L;

    /**
     * 数据类型
     */
    public int itemType = 0;
}

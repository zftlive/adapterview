package com.zftlive.share.adapterview.adapter;

import android.content.Context;
import android.graphics.Color;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.zftlive.share.adapterview.base.BasicAdapter;
import com.zftlive.share.adapterview.GlobalApplication;
import com.zftlive.share.adapterview.IAdapterViewType;
import com.zftlive.share.adapterview.bean.ArticleRowData;
import com.zftlive.share.adapterview.bean.ButtonLinkBean;
import com.zftlive.share.adapterview.bean.MutilTypeRowBean;

import share.zftlive.com.adapterview.R;

/**
 * 多种类型列表适配器
 *
 * @author 曾繁添
 * @version 1.0
 */
public class MutilTypeAdapter extends BasicAdapter implements IAdapterViewType {

    /**
     * 图片显示option
     */
    protected DisplayImageOptions mExactlyOption,mRoundOption;

    public MutilTypeAdapter(Context mContext) {
        super(mContext);
        mExactlyOption = GlobalApplication.gainExactlyOption(R.mipmap.common_module_default_picture);
        mRoundOption = GlobalApplication.getRoundOptions(R.mipmap.common_module_default_user_avtar);
    }

    @Override
    public int getViewTypeCount() {
        //返回一共有多少种类型的item
        return 2;
    }

    @Override
    public int getItemViewType(int position) {
        return ((MutilTypeRowBean) getItem(position)).itemType;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        ArticleViewHolder mArticleHoder = null;
        ButtonLinkViewHolder mButtonLinkHoder = null;

        int viewType = getItemViewType(position);
        //渲染布局UI以及复用
        if (null == convertView) {
            switch (viewType){
                case TYPE_BUTTON_LINK:
                    convertView = LayoutInflater.from(getContext()).inflate(R.layout.element_item_button_link,parent,false);
                    mButtonLinkHoder = new ButtonLinkViewHolder();
                    mButtonLinkHoder.mIcon = (ImageView)convertView.findViewById(R.id.iv_button_icon);
                    mButtonLinkHoder.mButtonText = (TextView) convertView.findViewById(R.id.tv_button_text);
                    convertView.setTag(mButtonLinkHoder);
                    break;
                case TYPE_ARTICLE:
                    convertView = LayoutInflater.from(getContext()).inflate(R.layout.element_item_article,parent,false);
                    mArticleHoder = new ArticleViewHolder();
                    //上下分割线
                    mArticleHoder.mTopSapce = convertView.findViewById(R.id.top_space);
                    mArticleHoder.mButtomSpace = convertView.findViewById(R.id.buttom_space);
                    //作者区域
                    mArticleHoder.mTopAuthorGroup = (ViewGroup) convertView.findViewById(R.id.ll_author_tag);
                    mArticleHoder.mAuthorImg = (ImageView) convertView.findViewById(R.id.iv_author_avatar);
                    mArticleHoder.mAuthorName = (TextView) convertView.findViewById(R.id.tv_author_name);
                    mArticleHoder.mReleaseDT = (TextView) convertView.findViewById(R.id.tv_release_datetime);
                    //文章标题、缩略图
                    mArticleHoder.mArticleThumb = (ImageView) convertView.findViewById(R.id.iv_list_thumb);
                    mArticleHoder.mTitle = (TextView) convertView.findViewById(R.id.tv_article_title);
                    mArticleHoder.mSummary = (TextView) convertView.findViewById(R.id.tv_summary);
                    mArticleHoder.mTagLabel = (TextView) convertView.findViewById(R.id.tv_label);
                    // 缓存Holder
                    convertView.setTag(mArticleHoder);
                    break;
            }
        }else{
            switch (viewType) {
                case TYPE_BUTTON_LINK:
                    mButtonLinkHoder = (ButtonLinkViewHolder) convertView.getTag();
                    break;
                case TYPE_ARTICLE:
                    mArticleHoder  = (ArticleViewHolder) convertView.getTag();
                    break;
            }
        }

        //填充数据
        final MutilTypeRowBean rowData = (MutilTypeRowBean) getItem(position);
        switch (viewType){
            case TYPE_BUTTON_LINK:
                ButtonLinkBean btnLink = rowData.buttonLink;
                ImageLoader.getInstance().displayImage(btnLink.iconURL,mButtonLinkHoder.mIcon,mExactlyOption);
                mButtonLinkHoder.mIcon.setVisibility(TextUtils.isEmpty(btnLink.iconURL)?View.GONE:View.VISIBLE);
                mButtonLinkHoder.mButtonText.setText(btnLink.buttonText);
                mButtonLinkHoder.mButtonText.setTextColor(Color.parseColor(btnLink.buttonTextColor));
                break;
            case TYPE_ARTICLE:
                ArticleRowData article = rowData.article;
                //第一个不显示顶部的线
                mArticleHoder.mTopSapce.setVisibility(0 == position ? View.GONE :View.VISIBLE);
                ImageLoader.getInstance().displayImage(article.authorImg,mArticleHoder.mAuthorImg,mRoundOption);
                mArticleHoder.mAuthorName.setText(article.authorName);
                mArticleHoder.mReleaseDT.setText(article.releaseDT);
                if(!TextUtils.isEmpty(article.thumb)){
                    ImageLoader.getInstance().displayImage(article.thumb,mArticleHoder.mArticleThumb,mExactlyOption);
                }
                mArticleHoder.mTitle.setText(article.title);
                mArticleHoder.mSummary.setText(article.summary);
                mArticleHoder.mTagLabel.setText(article.tag);
                mArticleHoder.mTagLabel.setVisibility(TextUtils.isEmpty(article.tag)?View.GONE:View.VISIBLE);
                break;
        }

        return convertView;
    }

    class ArticleViewHolder {
        /**
         * 上下占位间隙
         */
        View mTopSapce, mButtomSpace;

        /**
         * 顶部作者区域
         */
        ViewGroup mTopAuthorGroup;

        /**
         * 作者头像、文章缩略图
         */
        ImageView mAuthorImg, mArticleThumb;

        /**
         * 作者名称、发布时间、标题、摘要、类别标签
         */
        TextView mAuthorName, mReleaseDT, mTitle, mSummary, mTagLabel;
    }

    class ButtonLinkViewHolder{
        /**
         * 图标
         */
        private ImageView mIcon;

        /**
         * 按钮文案
         */
        private TextView mButtonText;
    }
}

package com.zftlive.share.adapterview;

/**
 * 列表类型枚举
 *
 * @author 曾繁添
 * @version 1.0
 */
public interface IAdapterViewType {

    /**
     * 按钮链接类型
     */
    int TYPE_BUTTON_LINK = 0;

    /**
     * 文章类型
     */
    int TYPE_ARTICLE = 1;
}

package com.zftlive.share.adapterview.adapter;

import android.content.Context;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.zftlive.share.adapterview.base.BasicAdapter;
import com.zftlive.share.adapterview.GlobalApplication;
import com.zftlive.share.adapterview.bean.ArticleRowData;

import share.zftlive.com.adapterview.R;

/**
 * 【精简优化】文章列表适配器
 *
 * @author 曾繁添
 * @version 1.0
 */
public class SmartArticleAdapter extends BasicAdapter {

    private DisplayImageOptions mRoundOption,mFadeOption;

    public SmartArticleAdapter(Context mContext) {
        super(mContext);
        mRoundOption = GlobalApplication.getRoundOptions(R.mipmap.common_module_default_user_avtar);
        mFadeOption = GlobalApplication.getFadeOptions(R.mipmap.common_module_default_picture);
    }

    @Override
    public View getView(final int position, View convertView,
                        ViewGroup parent) {
        ViewHolder holder = null;
        // 查找控件
        if (null == convertView) {
            convertView = LayoutInflater.from(getContext()).inflate(R.layout.element_item_article, parent, false);
            holder = new ViewHolder();
            //上下分割线
            holder.mTopSapce = convertView.findViewById(R.id.top_space);
            holder.mButtomSpace = convertView.findViewById(R.id.buttom_space);
            //作者区域
            holder.mTopAuthorGroup = (ViewGroup) convertView.findViewById(R.id.ll_author_tag);
            holder.mAuthorImg = (ImageView) convertView.findViewById(R.id.iv_author_avatar);
            holder.mAuthorName = (TextView) convertView.findViewById(R.id.tv_author_name);
            holder.mReleaseDT = (TextView) convertView.findViewById(R.id.tv_release_datetime);
            //文章标题、缩略图
            holder.mArticleThumb = (ImageView) convertView.findViewById(R.id.iv_list_thumb);
            holder.mTitle = (TextView) convertView.findViewById(R.id.tv_article_title);
            holder.mSummary = (TextView) convertView.findViewById(R.id.tv_summary);
            holder.mTagLabel = (TextView) convertView.findViewById(R.id.tv_label);
            // 缓存Holder
            convertView.setTag(holder);
        } else {
            holder = (ViewHolder) convertView.getTag();
        }

        // 设置数据
        final ArticleRowData rowData = (ArticleRowData) getItem(position);
        //第一个不显示顶部的线
        holder.mTopSapce.setVisibility(0 == position ? View.GONE :View.VISIBLE);
        ImageLoader.getInstance().displayImage(rowData.authorImg,holder.mAuthorImg,mRoundOption);
        holder.mAuthorName.setText(rowData.authorName);
        holder.mReleaseDT.setText(rowData.releaseDT);
        if(!TextUtils.isEmpty(rowData.thumb)){
            ImageLoader.getInstance().displayImage(rowData.thumb,holder.mArticleThumb,mFadeOption);
        }
        holder.mTitle.setText(rowData.title);
        holder.mSummary.setText(rowData.summary);
        holder.mTagLabel.setText(rowData.tag);
        holder.mTagLabel.setVisibility(TextUtils.isEmpty(rowData.tag)?View.GONE:View.VISIBLE);

        return convertView;
    }

    class ViewHolder {
        /**
         * 上下占位间隙
         */
        View mTopSapce, mButtomSpace;

        /**
         * 顶部作者区域
         */
        ViewGroup mTopAuthorGroup;

        /**
         * 作者头像、文章缩略图
         */
        ImageView mAuthorImg, mArticleThumb;

        /**
         * 作者名称、发布时间、标题、摘要、类别标签
         */
        TextView mAuthorName, mReleaseDT, mTitle, mSummary, mTagLabel;
    }
}

package com.zftlive.share.adapterview.ui;

import android.os.Bundle;
import android.support.v4.app.FragmentActivity;
import android.support.v4.widget.SwipeRefreshLayout;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.Toast;

import com.zftlive.share.adapterview.base.BasicAdapter;
import com.zftlive.share.adapterview.IAdapterViewType;
import com.zftlive.share.adapterview.adapter.ArticleListAdapter;
import com.zftlive.share.adapterview.adapter.MutilTypeAdapter;
import com.zftlive.share.adapterview.adapter.SmartArticleAdapter;
import com.zftlive.share.adapterview.adapter.SmartMutilTypeAdapter;
import com.zftlive.share.adapterview.bean.ArticleRowData;
import com.zftlive.share.adapterview.bean.ButtonLinkBean;
import com.zftlive.share.adapterview.bean.MutilTypeRowBean;

import java.util.ArrayList;
import java.util.List;

import share.zftlive.com.adapterview.R;

/**
 * 主界面
 *
 * @author 曾繁添
 * @version 1.0
 */
public class MainActivity extends FragmentActivity implements SwipeRefreshLayout.OnRefreshListener, AdapterView.OnItemClickListener {


    /**
     * 下拉刷新控件
     */
    private SwipeRefreshLayout mSwipeLayout;

    /**
     * 列表
     */
    private ListView mListview;

    /**
     * 列表适配器
     */
    private ArticleListAdapter mAdapter;

    /**
     * 精简列表适配器
     */
    private BasicAdapter mBasicAdapter;

    /**
     * 多类型Adapter
     */
    private MutilTypeAdapter mMutilTypeAdapter;

    /**
     * 视图模板分离多类型Adapter
     */
    private SmartMutilTypeAdapter mMutilTempletAdapter;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        initView();
        initData();
    }

    @Override
    public void onRefresh() {

        //下拉刷新
        mSwipeLayout.postDelayed(new Runnable() {
            @Override
            public void run() {
                if (mSwipeLayout.isRefreshing()) {
                    initData();
                    mSwipeLayout.setRefreshing(false);
                }
            }
        }, 200);
    }

    /**
     * 初始化控件
     */
    private void initView() {
        mSwipeLayout = (SwipeRefreshLayout) findViewById(R.id.swipe_refresh);
        mSwipeLayout.setColorSchemeResources(R.color.anl_blue_359df5_60_alpha);
        mSwipeLayout.setOnRefreshListener(this);

//        mAdapter = new ArticleListAdapter(this);
//        mListview.setAdapter(mAdapter);

        mBasicAdapter = new SmartArticleAdapter(this);
        mListview = (ListView) findViewById(R.id.listview);
        mListview.setOnItemClickListener(this);
//        mListview.setAdapter(mBasicAdapter);

//        //多类型Adapter
//        mMutilTypeAdapter = new MutilTypeAdapter(this);
//        mListview.setAdapter(mMutilTypeAdapter);

        //视图模板分离多类型Adapter
        mMutilTempletAdapter = new SmartMutilTypeAdapter(this);
        mListview.setAdapter(mMutilTempletAdapter);

    }

    /**
     * 初始化数据
     */
    private void initData() {
//        List<ArticleRowData> moniData = new ArrayList<>();
//        for (int i = 0; i < 50; i++) {
//            ArticleRowData article = new ArticleRowData();
//            article.authorImg = "http://zftlive-images.qiniudn.com/avtar-300x300.jpg";
//            article.authorName = "Ajava攻城师";
//            article.releaseDT = "30分钟之前";
//            article.title = (i + 1) + "东方时尚驾校-C1驾驶证";
//            article.summary = (i + 1) + "侧方和倒车入库都跟坐姿很大关系，调整座椅和镜子尤其重要！座椅和镜子尤其重要！座椅和镜子尤其重要！";
//            article.thumb = "http://img.ivsky.com/img/tupian/t/201610/10/qiuri_de_fengjing.jpg";
//            article.tag = "驾驶证";
//            moniData.add(article);
//        }
        //先清除历史数据
//        mBasicAdapter.clear();
//        mAdapter.setDataSource(moniData);
//        mAdapter.notifyDataSetChanged();

//        mBasicAdapter.addItem(moniData);
//        mBasicAdapter.notifyDataSetChanged();

        List<MutilTypeRowBean> moniData = new ArrayList<>();
        for (int i = 0; i < 50; i++) {
            MutilTypeRowBean rowData = new MutilTypeRowBean();
            if (i % 2 == 0) {
                rowData = new MutilTypeRowBean(IAdapterViewType.TYPE_ARTICLE, new ArticleRowData());
                rowData.article.authorImg = "http://zftlive-images.qiniudn.com/avtar-300x300.jpg";
                rowData.article.authorName = "Ajava攻城师";
                rowData.article.releaseDT = "30分钟之前";
                rowData.article.title = (i + 1) + "东方时尚驾校-C1驾驶证";
                rowData.article.summary = (i + 1) + "侧方和倒车入库都跟坐姿很大关系，调整座椅和镜子尤其重要！座椅和镜子尤其重要！座椅和镜子尤其重要！";
                rowData.article.thumb = "http://img.ivsky.com/img/tupian/t/201610/10/qiuri_de_fengjing.jpg";
                rowData.article.tag = "驾驶证";
            } else {
                rowData = new MutilTypeRowBean(IAdapterViewType.TYPE_BUTTON_LINK, new ButtonLinkBean("", (i + 1) + " 查看更多", "#359df5"));
            }
            moniData.add(rowData);
        }

//        //多类型item模拟数据
//        mMutilTypeAdapter.clear();
//        mMutilTypeAdapter.addItem(moniData);
//        mMutilTypeAdapter.notifyDataSetChanged();

        //视图模板分离多类型
        mMutilTempletAdapter.clear();
        mMutilTempletAdapter.addItem(moniData);
        mMutilTempletAdapter.notifyDataSetChanged();

    }

    @Override
    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
        //当前行数据
        Object rowData = parent.getItemAtPosition(position);
        if (null == rowData) {
            return;
        }
        Toast.makeText(this, "点击了" + rowData, Toast.LENGTH_SHORT).show();
    }
}

package com.zftlive.share.adapterview;

import android.app.Application;
import android.content.Context;
import android.graphics.Bitmap;

import com.nostra13.universalimageloader.cache.disc.impl.UnlimitedDiskCache;
import com.nostra13.universalimageloader.cache.disc.naming.HashCodeFileNameGenerator;
import com.nostra13.universalimageloader.cache.memory.impl.UsingFreqLimitedMemoryCache;
import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.ImageLoaderConfiguration;
import com.nostra13.universalimageloader.core.assist.ImageScaleType;
import com.nostra13.universalimageloader.core.assist.QueueProcessingType;
import com.nostra13.universalimageloader.core.decode.BaseImageDecoder;
import com.nostra13.universalimageloader.core.display.FadeInBitmapDisplayer;
import com.nostra13.universalimageloader.core.display.RoundedBitmapDisplayer;
import com.nostra13.universalimageloader.core.download.BaseImageDownloader;
import com.nostra13.universalimageloader.utils.StorageUtils;

import java.io.File;

/**
 * 自定义全局Application
 *
 * @author 曾繁添
 * @version 1.0
 */
public class GlobalApplication extends Application {

    @Override
    public void onCreate() {
        super.onCreate();

        initImageLoader(this);
    }

    /**
     * 初始化ImageLoader
     *
     * @param mContext 上下文，建议用Application
     * @return
     */
    public static ImageLoader initImageLoader(Context mContext) {
        // 获取到缓存的目录地址
        File cacheDir = StorageUtils.getOwnCacheDirectory(mContext, "ImageLoader/Cache");
        ImageLoaderConfiguration config =
                new ImageLoaderConfiguration.Builder(mContext.getApplicationContext())
                        // .memoryCacheExtraOptions(1280, 1920) // default = device screen dimensions
                        // .diskCacheExtraOptions(1280, 1920, null)
                        .threadPoolSize(3)
                        // default
                        .threadPriority(Thread.NORM_PRIORITY - 2)
                        // default
                        .tasksProcessingOrder(QueueProcessingType.FIFO)
                        // default ，不需要多个尺寸大小在内存中
                        // .denyCacheImageMultipleSizesInMemory()
                        /**
                         * UIL中的内存缓存策略 1. 只使用的是强引用缓存
                         * LruMemoryCache（这个类就是这个开源框架默认的内存缓存类，缓存的是bitmap的强引用，下面我会从源码上面分析这个类） 2.使用强引用和弱引用相结合的缓存有
                         * UsingFreqLimitedMemoryCache（如果缓存的图片总量超过限定值，先删除使用频率最小的bitmap）
                         * LRULimitedMemoryCache（这个也是使用的lru算法，和LruMemoryCache不同的是，他缓存的是bitmap的弱引用）
                         * FIFOLimitedMemoryCache（先进先出的缓存策略，当超过设定值，先删除最先加入缓存的bitmap）
                         * LargestLimitedMemoryCache(当超过缓存限定值，先删除最大的bitmap对象) LimitedAgeMemoryCache（当
                         * bitmap加入缓存中的时间超过我们设定的值，将其删除） 3.只使用弱引用缓存
                         * WeakMemoryCache（这个类缓存bitmap的总大小没有限制，唯一不足的地方就是不稳定，缓存的图片容易被回收掉）
                         */
                        .memoryCache(new UsingFreqLimitedMemoryCache(8 * 1024 * 1024))
                        // 限制内存大小
                        .memoryCacheSize(8 * 1024 * 1024)
                        .memoryCacheSizePercentage(10)
                        // default
                        .diskCache(new UnlimitedDiskCache(cacheDir)) // default
                        .diskCacheSize(50 * 1024 * 1024).diskCacheFileCount(100)
                        .diskCacheFileNameGenerator(new HashCodeFileNameGenerator()) // default
                        .imageDownloader(new BaseImageDownloader(mContext)) // default
                        .imageDecoder(new BaseImageDecoder(true)) // default
                        .defaultDisplayImageOptions(DisplayImageOptions.createSimple()) // default
                        .build();

        // 全局初始化此配置
        ImageLoader.getInstance().init(config);

        return ImageLoader.getInstance();
    }

    /**
     * 圆形加载图片
     *
     * @param defaultImage 默认图片
     * @return
     */
    public static DisplayImageOptions getRoundOptions(int defaultImage) {
        return
                new DisplayImageOptions.Builder()
                        .displayer(new RoundedBitmapDisplayer(320))
                        .bitmapConfig(Bitmap.Config.RGB_565)
                        .showImageOnFail(defaultImage)
                        .showImageForEmptyUri(defaultImage)
                        .imageScaleType(ImageScaleType.EXACTLY)
                        .cacheInMemory(true)
                        .cacheOnDisk(true).build();
    }

    /**
     * 获取渐现显示选项
     *
     * @param defaultImgResId 加载/出错/空时默认图
     * @return
     */
    public static DisplayImageOptions getFadeOptions(int defaultImgResId) {
        DisplayImageOptions options = new DisplayImageOptions.Builder()
                // 设置图片加载/解码过程中错误时候显示的图片
                .showImageOnFail(defaultImgResId)
                // 设置图片Uri为空或是错误的时候显示的图片
                .showImageForEmptyUri(defaultImgResId)
                // 设置下载的图片是否缓存在内存中
                .cacheInMemory(true)
                // 设置下载的图片是否缓存在SD卡中
                .cacheOnDisk(true)
                /**设置图片缩放方式：
                 EXACTLY :图像将完全按比例缩小到目标大小
                 EXACTLY_STRETCHED:图片会缩放到目标大小完全
                 IN_SAMPLE_INT:图像将被二次采样的整数倍
                 IN_SAMPLE_POWER_OF_2:图片将降低2倍，直到下一减少步骤，使图像更小的目标大小
                 NONE:图片不会调整
                 ***/
                .imageScaleType(ImageScaleType.EXACTLY)
                // 设置图片的解码类型
                .bitmapConfig(Bitmap.Config.RGB_565)
                // 设置图片下载前的延迟
                .delayBeforeLoading(100)
                // delayInMillis为你设置的延迟时间
                // 设置图片加入缓存前，对bitmap进行设置
                // .preProcessor(BitmapProcessor preProcessor)
                /**
                 * 图片显示方式：
                 *  RoundedBitmapDisplayer（int roundPixels）设置圆角图片
                 *  FakeBitmapDisplayer（）这个类什么都没做
                 *  FadeInBitmapDisplayer（int durationMillis）设置图片渐显的时间
                 　　　　  	 *　   SimpleBitmapDisplayer()正常显示一张图片
                 **/
                .displayer(new FadeInBitmapDisplayer(300))// 渐显--设置图片渐显的时间
                .build();
        return options;
    }


    /**
     * EXACTLY类型option-RGB_565
     */
    public static DisplayImageOptions gainExactlyOption(int defaultResId) {
        return new DisplayImageOptions.Builder()
                .showImageForEmptyUri(defaultResId)
                .showImageOnFail(defaultResId)
                .bitmapConfig(Bitmap.Config.RGB_565)
                .imageScaleType(ImageScaleType.EXACTLY)
                .cacheInMemory(true)
                .cacheOnDisk(true).build();
    }
}

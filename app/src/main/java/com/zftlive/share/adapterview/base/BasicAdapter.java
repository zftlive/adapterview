package com.zftlive.share.adapterview.base;

import android.content.Context;
import android.widget.BaseAdapter;

import com.zftlive.share.adapterview.base.IAdapterModel;

import java.lang.ref.WeakReference;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

/**
 * 基础适配器
 *
 * @author 曾繁添
 * @version 1.0
 */
public abstract class BasicAdapter extends BaseAdapter {

    /**
     * 托管数据源
     */
    private List<IAdapterModel> mDataSource = new ArrayList();

    /**
     * 持有Context的软引用,防止内存泄露
     */
    protected WeakReference<Context> mWeakRefContext;

    public BasicAdapter(Context mContext){
        this.mWeakRefContext = new WeakReference<Context>(mContext);
    }

    @Override
    public int getCount() {
        return mDataSource.size();
    }

    @Override
    public IAdapterModel getItem(int position) {
        if (null != mDataSource && position < mDataSource.size()) {
            return mDataSource.get(position);
        }
        return null;
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    /**
     * 获取当前ListView绑定的数据源
     * @return
     */
    public List gainDataSource(){
        return mDataSource;
    }

    /**
     * 添加数据
     * @param object 数据模型
     */
    public boolean addItem(IAdapterModel object){
        return mDataSource.add(object);
    }

    /**
     * 在指定索引位置添加数据
     * @param location 索引
     * @param object 数据模型
     */
    public void addItem(int location,IAdapterModel object){
        mDataSource.add(location, object);
    }

    /**
     * 集合方式添加数据
     * @param collection 集合
     */
    public boolean addItem(Collection collection){
        return mDataSource.addAll(collection);
    }

    /**
     * 在指定索引位置添加数据集合
     * @param location 索引
     * @param collection 数据集合
     */
    public boolean addItem(int location,Collection<? extends IAdapterModel> collection){
        return mDataSource.addAll(location,collection);
    }

    /**
     * 移除指定对象数据
     * @param object 移除对象
     * @return 是否移除成功
     */
    public boolean removeItem(IAdapterModel object){
        return mDataSource.remove(object);
    }

    /**
     * 移除指定索引位置对象
     * @param location 删除对象索引位置
     * @return 被删除的对象
     */
    public Object removeItem(int location){
        return mDataSource.remove(location);
    }

    /**
     * 移除指定集合对象
     * @param collection 待移除的集合
     * @return 是否移除成功
     */
    public boolean removeAll(Collection<? extends IAdapterModel> collection){
        return mDataSource.removeAll(collection);
    }

    /**
     * 清空数据
     */
    public void clear() {
        mDataSource.clear();
    }

    /**
     * 获取Context上下文
     * @return
     */
    public Context getContext(){
        return mWeakRefContext.get();
    }
}

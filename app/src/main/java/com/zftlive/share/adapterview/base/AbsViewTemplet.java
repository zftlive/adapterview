package com.zftlive.share.adapterview.base;

import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import java.lang.reflect.Constructor;
import java.lang.reflect.InvocationTargetException;

/**
 * 模板基础抽象接口定义
 * <p/>
 * inflate-->bindLayout或者bindView-->initView-->fillData
 *
 * @author 曾繁添
 * @version 1.0
 */
public abstract class AbsViewTemplet implements IViewTemplet,View.OnClickListener {

    /**
     * 上下文
     */
    protected Context mContext;

    /**
     * 当前item对应的view视图
     */
    protected View mLayoutView;

    /**
     * 当前item对应的type类型
     */
    protected int viewType;

    /**
     * 当前位置索引
     */
    protected int position;

    /**
     * 当前item数据
     */
    protected IAdapterModel data;

    /**
     * 日志输出标识
     */
    protected final String TAG = this.getClass().getSimpleName();

    public AbsViewTemplet(Context mContext) {
        this.mContext = mContext;
    }

    /**
     * 渲染UI布局
     *
     * @param viewType item对应的类型
     * @param postion  当前数据行位置索引
     * @param parent   根节点，没有可以传null
     * @return 当前模板渲染的View
     */
    public View inflate(int viewType, int postion, ViewGroup parent) {
        this.viewType = viewType;
        this.position = postion;

        int layout = bindLayout();
        if (null != parent) {
            mLayoutView = LayoutInflater.from(mContext).inflate(layout, parent, false);
        } else {
            mLayoutView = LayoutInflater.from(mContext).inflate(layout, parent);
        }
        mLayoutView.setOnClickListener(this);

        return mLayoutView;
    }

    /**
     * 缓存住当前行对应的参数,供基类adapter的getView方法中调用
     *
     * @param viewType item对应的类型
     * @param postion  当前数据所在位置索引
     * @param data     当前行数据
     */
    void holdCurrentParams(int viewType, int postion, IAdapterModel data) {
        this.viewType = viewType;
        this.position = postion;
        this.data = data;
    }

    @Override
    public void onClick(View v) {
        try {
            itemClick(v, position, data);
        } catch (Exception e) {
            e.printStackTrace();
            Log.d(TAG, "点击跳转发生异常，原因：" + e.getMessage());
        }
    }

    @Override
    public View bindView() {
        return null;
    }

    /**
     * item点击事件
     *
     * @param view    当前点击的view
     * @param postion 当前行位置索引
     * @param rowData 当前行数据模型
     */
    public void itemClick(View view, int postion, IAdapterModel rowData) {
        Toast.makeText(mContext, "点击了" + rowData, Toast.LENGTH_SHORT).show();
    }

    /**
     * 获取当前item渲染的view
     *
     * @return
     */
    public View getItemLayoutView() {
        return mLayoutView;
    }

    /**
     * 查找控件
     *
     * @param id
     * @return
     */
    protected View findViewById(int id) {
        if (null != mLayoutView) {
            return mLayoutView.findViewById(id);
        }
        return null;
    }

    /**
     * 构建模板实例
     *
     * @param mViewTemplet
     * @param arguments    构造方法形参
     * @param <D>
     * @return
     * @throws Exception
     */
    public static <D extends AbsViewTemplet> D createViewTemplet(Class<D> mViewTemplet, Object... arguments) {
        Constructor<?> constructor = findConstructor(mViewTemplet, arguments);
        D mTemplet = null;
        try {
            mTemplet = (D) constructor.newInstance(arguments);
        } catch (InstantiationException e) {
            e.printStackTrace();
        } catch (IllegalAccessException e) {
            e.printStackTrace();
        } catch (InvocationTargetException e) {
            e.printStackTrace();
        }
        return mTemplet;
    }

    /**
     * 匹配构造器
     *
     * @param mClass
     * @param params
     * @return
     */
    private static Constructor<?> findConstructor(Class<?> mClass, Object... params) {
        for (Constructor<?> constructor : mClass.getConstructors()) {
            Class<?>[] paramsTypes = constructor.getParameterTypes();
            if (paramsTypes.length == params.length) {
                boolean match = true;
                for (int i = 0; i < paramsTypes.length; i++) {
                    if (!paramsTypes[i].isAssignableFrom(params[i].getClass())) {
                        match = false;
                        break;
                    }
                }
                if (match) {
                    return constructor;
                }
            }
        }
        return null;
    }
}

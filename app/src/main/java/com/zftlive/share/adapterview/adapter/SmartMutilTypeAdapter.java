package com.zftlive.share.adapterview.adapter;

import android.content.Context;

import com.zftlive.share.adapterview.IAdapterViewType;
import com.zftlive.share.adapterview.base.AbsMutilTypeAdapter;
import com.zftlive.share.adapterview.base.AbsViewTemplet;
import com.zftlive.share.adapterview.base.IAdapterModel;
import com.zftlive.share.adapterview.bean.MutilTypeRowBean;
import com.zftlive.share.adapterview.templet.ArticleViewTemplet;
import com.zftlive.share.adapterview.templet.ButtonLinkViewTemplet;

import java.util.Map;

/**
 * 【多类型】列表适配器
 *
 * @author 曾繁添
 * @version 1.0
 */
public class SmartMutilTypeAdapter extends AbsMutilTypeAdapter implements IAdapterViewType {

    public SmartMutilTypeAdapter(Context mContext) {
        super(mContext);
    }

    @Override
    protected void registeViewTemplet(Map<Integer, Class<? extends AbsViewTemplet>> mViewTemplet) {
        mViewTemplet.put(IAdapterViewType.TYPE_ARTICLE,ArticleViewTemplet.class);
        mViewTemplet.put(IAdapterViewType.TYPE_BUTTON_LINK,ButtonLinkViewTemplet.class);
    }

    @Override
    protected int adjustItemViewType(IAdapterModel model, int position) {
        if(null != model && model instanceof MutilTypeRowBean){
            return ((MutilTypeRowBean)model).itemType;
        }
        return 0;
    }
}
